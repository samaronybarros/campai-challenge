const secrets = {
    dbUri: "mongodb://localhost/campai-challenge"
};

export const getSecret = key => secrets[key];