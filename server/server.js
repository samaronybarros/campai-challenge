import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import mongoose from 'mongoose';

import { getSecret } from './secret';
/*
import Orgs from './models/Orgs';
import Contacts from './models/Contacts';
import Groups from './models/Groups';
*/

var cors = require('cors')

// Creating instances
const app = express();
const router = express.Router();

// Setting port to either a predetermined port number if it have set it up, or 3001
const API_PORT = process.env.API_PORT || 3001;

// DB config -- Setting URI from secrets.js
mongoose.connect(getSecret('dbUri'), { useNewUrlParser: true });
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Configuring the API to use bodyParser and looking for JSON data in the request body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

app.use(cors())

// Setting the route path & initializing the API
router.get('/', (req, res) => {
    res.json({ message: 'Testing connection: OK!' });
});

//Defining the CRUD Session - Create/Read/Update/Delete
//GET - Endpoint (Read)

//mongoose.models
var fs = require('fs');
var files = fs.readdirSync('models');

Array.prototype.forEach.call(files, function(file, i) {
    var fileName = file.split('.',1).toString();
    var apiName = '/' + fileName.toLowerCase();
    var modelPath = './models/' + fileName;

    var schema = require(modelPath);
    var obj = null;

    if (fileName === 'Groups' ) {
        var obj = mongoose.model('ContactGroups', schema);
    } else {
        var obj = mongoose.model(fileName, schema);
    }

    router.get(apiName, (req, res) => {
        obj.find((err, schema) => {
            if (err) {
                return res.json({ success: false, error: err });
            }
            return res.json({ success: true, data: schema });
        });
    });
}); 

/*
router.get('/orgs', (req, res) => {
    Orgs.find((err, orgs) => {
        if (err) {
            return res.json({ success: false, error: err });
        }
        return res.json({ success: true, data: orgs });
    });
});

router.get('/contacts', (req, res) => {
    Contacts.find((err, contacts) => {
        if (err) {
            return res.json({ success: false, error: err });
        }
        return res.json({ success: true, data: contacts });
    });
});

router.get('/groups', (req, res) => {
    Groups.find((err, groups) => {
        if (err) {
            return res.json({ success: false, error: err });
        }
        return res.json({ success: true, data: groups });
    });
});
*/

//POST - Endpoint (Create)
router.post('/orgs', (req, res) => {
    const orgs = new Orgs();
    const { city, name, type } = req.body;
    if (!city || !name || !type) {
        return res.json({
            success: false,
            error: 'You must provide a city, a name and a type'
        });
    }
    orgs.city = city;
    orgs.name = name;
    orgs.type = type;
    orgs.save(err => {
        if (err) {
            return res.json({ success: false, error: err });
        }
        return res.json({ success: true });
    });
});

//PUT - Endpoint (Update)
router.put('/orgs/:orgsId', (req, res) => {
    const { orgsId } = req.params;

    if (!orgsId) {
        return res.json({ success: false, error: 'No orgs id provided' });
    }
    Orgs.findById(orgsId, (error, orgs) => {
        if (error) return res.json({ success: false, error });
        const { author, text } = req.body;

        if (city) {
            orgs.city = city;
        }

        if (name) {
            orgs.name = name;
        }

        if (type) {
            orgs.type = type;
        }

        orgs.save(error => {
            if (error) {
                return res.json({ success: false, error });
            }
            return res.json({ success: true });
        });
    });
});

//DELETE - Endpoint (Delete)
router.delete('/orgs/:orgsId', (req, res) => {
    const { orgsId } = req.params;
    if (!orgsId) {
        return res.json({ success: false, error: 'No orgs id provided' });
    }
    Orgs.remove({ _id: orgsId }, (error, orgs) => {
        if (error) {
            return res.json({ success: false, error });
        }
        return res.json({ success: true });
    });
});

// Use router configuration when we call /api
app.use('/api', router);

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: {
            message: err.message
        }
    });
});

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    if (req.method === 'Options') {
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
        return res.status(200).json({});
    }
});

app.listen(API_PORT, () => console.log(`Listening on port ${API_PORT}`));