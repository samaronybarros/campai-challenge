import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const collectionSchema = new Schema({
  group: String,
  first_name: String,
  last_name: String,
}, { timestamps: true });