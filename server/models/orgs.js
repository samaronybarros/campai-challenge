import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const collectionSchema = new Schema({
  city: String,
  name: String,
  type: String,
}, { timestamps: true });