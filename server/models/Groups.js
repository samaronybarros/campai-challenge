import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const collectionSchema = new Schema({
  iban: String,
  name: String,
  account_holder: String,
}, { timestamps: true });