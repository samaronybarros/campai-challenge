import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const collectionSchema = new Schema({
  email: String,
  first_name: String,
  last_name: String,
}, { timestamps: true });