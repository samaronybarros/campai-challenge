# campai - Backend for Fullstack developer Challenge by Sam Barros

Welcome to **[campai](https://campai.com/)**!

This is the project developed by Sam Barros to get into The Campai company.

Here, we will present how to config the tool.

## What you should install?

For this project, I decided to use the MERN (MongoDB, Express.js, React.js and Node.js). 
![mern](http://www.brain-mentors.com/img/mern-training-institute.jpg)

*   Mongo 4.0.2+
*   Express.js 4.16.3+
*   React.js 16.5.0+
*   Node 8.12+

To code on this project, I used VS Code
*   VS Code 1.27

As a package manager
*   Yarn

Although the use of NPM was recommended, I preferred to use Yarn as package manager because it seemed more powerful to config. However, I saw after, NPM do the same thing, but to keep the previous configuration I decided, for this project, maintain the use of YARN

## Folders
Basically, I have **two** folders in the structure of the campai-challenge project, `server` and `client`. In the `client` directory are all the front-end files and in the `server` directory, back-end.

The structure is similar to that
```
- campai-challenge
    - client
        - node_modules
        - public
            - _css
        - src
            - Components
    - server
        - models
```

### Components
Here I show how I divide the Components in the project.

![figmaComponents](doc/img/figmaComponents.png)

## Dependencies
I used some packages to facilitate the development of the project, they are:

- **Express**: It is the server framework (The E in M __E__ RN)
- **Body parser**: Responsible to get the body off of network requests
- **Nodemon**: Restart the server when it sees changes (for a better dev experience)
- **Morgan**: Make easier to debug the network requests to this api
- **Mongoose**: Lets us interact with MongoDB in an easier way (the M in __M__ ERN)

Besides, we need some additional packages to execute some results.
- **react-markdown**: Convert markdown to text
- **whatwg-fetch**: Fetch data from the browser
- **eslint**: Get us the prop types, use the airbnb eslint rule
- **material-ui**: React UI framework
- **cors**: Package for providing a Connect/Express middleware that can be used to enable CORS with various options.
- **avataaars**: React component for Avataaars Generator 
- **styled-components**: Package to use CSS native on Javascript
- **fs-extra**: Adds file system methods that aren't included in the native fs module and adds promise support to the fs methods
```
$ yarn add react react-dom
$ yarn add express body-parser nodemon morgan mongoose concurrently babel-cli babel-preset-es2015 babel-preset-stage-0
$ yarn add react-markdown whatwg-fetch prop-types
$ yarn add --dev eslint babel-eslint
$ yarn add @material-ui/core
$ yarn add cors
$ yarn add avataaars
$ yarn add styled-components
$ yarn add fs-extra
```

Resuming:
```
$ yarn add --save react react-dom body-parser nodemon morgan mongoose concurrently babel-cli babel-preset-es2015 babel-preset-stage-0 react-markdown whatwg-fetch prop-types eslint babel-eslint @material-ui/core cors avataaars styled-components fs-extra
```


## Initiate
To use React (the R in ME __R__ N) I executed the following command:

```
$ create-react-app client
```

With Node.js installed (the N in MER__N__), we can server set up.

```javascript
//...
import mongoose from 'mongoose';
//...
mongoose.connect(getSecret('dbUri'), { useNewUrlParser: true });
//...
```

Here, I have the function `getSecret` responsible to get the `URL` to connect to the database. In the same folder, there is a file called `secret.js` that contains the `URL`.

```javascript
//...
const secrets = {
    dbUri: "<URL>"
};

export const getSecret = key => secrets[key];
//...
```

That file should be included on __.gitignore__ file.

### Server
To keep the code reusable to search all the field on the database I used a simple code like below.

```javascript
//...
var fs = require('fs');
var files = fs.readdirSync('models');

Array.prototype.forEach.call(files, function(file, i) {
    var fileName = file.split('.',1).toString();
    var apiName = '/' + fileName.toLowerCase();
    var modelPath = './models/' + fileName;

    var schema = require(modelPath);
    var obj = null;

    router.get(apiName, (req, res) => {
        obj.find((err, schema) => {
            if (err) {
                return res.json({ success: false, error: err });
            }
            return res.json({ success: true, data: schema });
        });
    });
}); 
//...
```

Below is the end-points' schema where you can just replace it and change the fields for that you would like to include in yours.

```javascript
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const colletctionSchema = new Schema({
  city: String,
  name: String,
  type: String,
}, { timestamps: true });
```

![postman](https://drive.google.com/uc?export=view&id=19nFE6jm756rde-EWFLVPMoxAyOfNZU50)

So, to creating another `API` to `GET` method, you can just create a new schema in __server/models__ directory.

![reusability](doc/gif/reusability.gif)

# Start Application
To Start the application, you need stay on the folder campai-challenge and type the command:

```
yarn start:dev
```
# Features
## Loading
When start the application, we can see a loading gif for each collection.
![loading](doc/gif/loading.gif)

## Searching
This is the input search working.
![searching](doc/gif/searching.gif)