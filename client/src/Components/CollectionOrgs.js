import React, { Component } from 'react';
import LettersAvatar from '@material-ui/core/Avatar';
import Avatar from '@material-ui/core/Avatar';
import 'whatwg-fetch';

import CollectionItem from './CollectionItem'
import CollectionField from './CollectionField'

class CollectionOrgs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataCollection: [],
            error: null,
            isLoading: false
        };

        this.pollInterval = null;

        this.componentDidMount.bind();
    }

    componentDidMount = () => {
        this.setState({ isLoading: true });

        fetch('/api/orgs')
            .then((data) => {
                return data.json();
            })
            .then((res) => {
                if (!res.success) {
                    this.setState({ error: res.error });
                } else {
                    this.setState({
                        isLoading: false,
                        dataCollection: res.data
                    });
                }
            }).catch(function (error) {
                console.log('[Orgs] There has been a problem with your fetch operation: ' + error.message);
            });

        if (!this.pollInterval) {
            this.pollInterval = setInterval(this.loadCommentsFromServer, 2000);
        }
    }

    componentWillUnmount() {
        if (this.pollInterval) {
            clearInterval(this.pollInterval);
        }
        this.pollInterval = null;
    }

    renderOrg(orgs) {
        var avatarCode = (orgs.logo
            ? <Avatar
                style={{ width: '45px', height: '45px' }}
                alt={orgs.name.substring(0, 1)}
                src={orgs.logo} />
            : <LettersAvatar>{orgs.name.substring(0, 1)}</LettersAvatar>)
        return (
            <CollectionField 
                id={orgs._id}
                avatarCode={avatarCode}
                city={orgs.city}
                name={orgs.name}
                type={orgs.type}
            />
        )
    }

    render() {
        let filter = []
        this.state.dataCollection.forEach((key) => {
            if (key.name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)
                || key.city.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)
                || key.type.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)) {
                filter.push(key)
            }

            return filter
        })

        const listItems = filter.map(this.renderOrg)

        return (
            <CollectionItem
                collection="Orgs"
                listItems={listItems}
                isLoading={this.state.isLoading}
            />
        );
    }
}

export default CollectionOrgs;