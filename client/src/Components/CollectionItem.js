import React, { Component } from 'react'

class CollectionItem extends Component {
    render() {
        return (
            <section>
                <li className="list-group-item item-bolded">{this.props.collection}</li>

                <div className="panel-body">
                    <div className="table-container">
                        <table
                            className="table-users table"
                            border="0">
                            <tbody id="table-orgs">
                                {
                                    this.props.isLoading &&
                                    <tr>
                                        <td>
                                            <img 
                                                src="_img/loading.gif" 
                                                alt=""
                                                height='100' width='auto'/>
                                                    Carregando...
                                        </td>
                                    </tr>
                                }
                                {
                                    !this.props.isLoading &&
                                    this.props.listItems}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        )
    }
}

export default CollectionItem