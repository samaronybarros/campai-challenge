import React, { Component } from 'react';

import CollectionOrgs from './CollectionOrgs';
import CollectionContacts from './CollectionContacts';
import CollectionGroups from './CollectionGroups';

class CollectionBox extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-6 col-md-offset-4 app shadow">
                    <ul className="list-group user-group">
                        <CollectionOrgs searchedText={this.props.searchedText} />
                        <CollectionContacts searchedText={this.props.searchedText}  />
                        <CollectionGroups searchedText={this.props.searchedText}  />
                    </ul>
                </div>
            </div>
        )
    }
}

export default CollectionBox;