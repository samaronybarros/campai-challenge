import React, { Component } from 'react';
import LettersAvatar from '@material-ui/core/Avatar';
import Avatar from 'avataaars'
import 'whatwg-fetch';

import CollectionItem from './CollectionItem'
import CollectionField from './CollectionField'

class CollectionContacts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataCollection: [],
            error: null,
            isLoading: false
        };

        this.pollInterval = null;

        this.componentDidMount.bind();
    }

    componentDidMount = () => {
        this.setState({ isLoading: true });

        fetch('/api/contacts')
            .then((data) => {
                return data.json();
            })
            .then((res) => {
                if (!res.success) {
                    this.setState({ error: res.error });
                } else {
                    this.setState({
                        isLoading: false,
                        dataCollection: res.data
                    });
                }
            }).catch(function (error) {
                console.log('[Contacts] There has been a problem with your fetch operation: ' + error.message);
            });

        if (!this.pollInterval) {
            this.pollInterval = setInterval(this.loadCommentsFromServer, 2000);
        }
    }

    componentWillUnmount() {
        if (this.pollInterval) {
            clearInterval(this.pollInterval);
        }
        this.pollInterval = null;
    }

    renderContact(contacts) {
        var avatarCode = (contacts.avatar ?
            <Avatar
                style={{ width: '45px', height: '45px' }}
                avatarStyle='Circle'
                topType={`${contacts.avatar.top_type}`}
                accessoriesType={`${contacts.avatar.accessories_type}`}
                clotheType={`${contacts.avatar.clothe_type}`}
                eyeType={`${contacts.avatar.eye_type}`}
                eyebrowType={`${contacts.avatar.eyebrow_type}`}
                mouthType={`${contacts.avatar.mouth_type}`}
                skinColor={`${contacts.avatar.skin_color}`}
            /> :
            <LettersAvatar>{contacts.first_name.substring(0, 1)}</LettersAvatar>)

        return (
            <CollectionField 
                id={contacts._id}
                avatarCode={avatarCode}
                city={contacts.address.city}
                name={contacts.first_name + " " + contacts.last_name}
                type={contacts.type}
            />
        )
    }

    render() {
        let filter = []
        this.state.dataCollection.forEach((key) => {
            if ((key.first_name + " " + key.last_name).normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)
                || key.address.city.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)
                || key.type.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)) {
                filter.push(key)
            }

            return filter
        })

        const listItems = filter.map(this.renderContact)

        return (
            <CollectionItem
                collection="Contacts"
                listItems={listItems}
                isLoading={this.state.isLoading}
            />
        );
    }
}

export default CollectionContacts;