import React from 'react';

import CollectionBox from './CollectionBox'

class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchedText: ""
        }

        this.handleChange.bind(this);
    }

    handleChange = e => {
        const newText = e.target.value;

        this.setState({
            searchedText: newText
        });
    };

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg">
                    <a className="navbar-brand" href={null}>campai</a>
                    <div className="row">
                        <div className="col-md-10">
                            <table>
                                <tbody>
                                    <tr>
                                        <td className="row">
                                            <div className="input-group">
                                                <input
                                                    id="search-input"
                                                    type="text"
                                                    className="form-control"
                                                    aria-describedby="search-addon"
                                                    onChange={this.handleChange} />
                                                <div className="input-group-append">
                                                    <span className="input-group-text icon-search" id="search-addon">
                                                        <i className="material-icons">
                                                            search
                                                    </i>
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <a className="navbar-brand" href={null}>Startseite</a>
                    <a className="navbar-brand" href={null}><span className="badge">17</span>Benachrichtigungen</a>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle"
                                    href={null} id="navbarDropdown"
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                    Alexander Adam
                            </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a className="dropdown-item" href={null}>My account</a>
                                    <a className="dropdown-item" href={null}>Log out</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <CollectionBox searchedText={this.state.searchedText} />
            </div>
        );
    }
}

export default SearchBox;