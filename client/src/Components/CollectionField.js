import React, { Component } from 'react'

class CollectionField extends Component {
    render() {
        return (
            <tr key={this.props.id}>
                <td width="10">
                    {this.props.avatarCode}
                </td>
                <td>
                    <span className="user-city">{this.props.city}</span>
                    <span className="user-block">{this.props.name}</span>
                    <span className="user-desc">{this.props.type}</span>
                </td>
            </tr>
        )
    }
}

export default CollectionField;