import React, { Component } from 'react';
import LettersAvatar from '@material-ui/core/Avatar';
import Avatar from 'avataaars'
import 'whatwg-fetch';

import CollectionItem from './CollectionItem'
import CollectionField from './CollectionField'

class CollectionGroups extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataCollection: [],
            error: null,
            isLoading: false
        };

        this.pollInterval = null;

        this.componentDidMount.bind();
    }

    componentDidMount = () => {
        this.setState({ isLoading: true });

        fetch('/api/groups')
            .then((data) => {
                return data.json();
            })
            .then((res) => {
                if (!res.success) {
                    this.setState({ error: res.error });
                } else {
                    this.setState({ 
                        isLoading : false,
                        dataCollection: res.data 
                    });
                }
            }).catch(function (error) {
                console.log('[Groups] There has been a problem with your fetch operation: ' + error.message);
            });

        if (!this.pollInterval) {
            this.pollInterval = setInterval(this.loadCommentsFromServer, 2000);
        }
    }

    componentWillUnmount() {
        if (this.pollInterval) {
            clearInterval(this.pollInterval);
        }
        this.pollInterval = null;
    }

    renderGroup(groups) {
        var avatarCode = (groups.avatar ?
            <Avatar
                style={{ width: '45px', height: '45px' }}
                avatarStyle='Circle'
                topType={`${groups.avatar.top_type}`}
                accessoriesType={`${groups.avatar.accessories_type}`}
                clotheType={`${groups.avatar.clothe_type}`}
                eyeType={`${groups.avatar.eye_type}`}
                eyebrowType={`${groups.avatar.eyebrow_type}`}
                mouthType={`${groups.avatar.mouth_type}`}
                skinColor={`${groups.avatar.skin_color}`}
            /> :
            <LettersAvatar>{groups.name.substring(0, 1)}</LettersAvatar>)

        return (

            <CollectionField 
                id={groups._id}
                avatarCode={avatarCode}
                city={groups.address.city}
                name={groups.name}
                type={`Community`}
            />
        )
    }

    render() {
        let filter = []
        this.state.dataCollection.forEach((key) => {
            if (key.name.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().match(this.props.searchedText)) {
                filter.push(key)
            }

            return filter
        })

        const listItems = filter.map(this.renderGroup)

        return (
            <CollectionItem
                collection="Groups"
                listItems={listItems}
                loading={this.isLoading}
            />
        );
    }
}

export default CollectionGroups;