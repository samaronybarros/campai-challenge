import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import SearchBox from './Components/SearchBox';

ReactDOM.render(<SearchBox />, document.getElementById('root'));
registerServiceWorker();
